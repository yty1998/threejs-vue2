module.exports = {
  devServer: {
    // 配置代理解决跨域问题
    proxy: {
      '/data1': {
        target: 'http://10.206.49.43:8089', // 要跨域的域名
        secure: true, // 接受对方是https的接口
        ws: true, // 是否启用websockets
        changeOrigin: true, // 是否允许跨越
        pathRewrite: {
          '^/data1': '' // 将你的地址代理为这个 /api 接下来请求时就使用这个/api来代替你的地址
        }
      },
      '/three-api': {
        target: 'http://10.206.49.210:10091/three-api', // 要跨域的域名
        secure: true, // 接受对方是https的接口
        ws: true, // 是否启用websockets
        changeOrigin: true, // 是否允许跨越
        pathRewrite: {
          '^/three-api': '' // 将你的地址代理为这个 /api 接下来请求时就使用这个/api来代替你的地址
        }
      }
    }
  },
  configureWebpack: config => {
    // 生产环境取消 console.log
    if (process.env.NODE_ENV === 'production') {
      config.optimization.minimizer[0].options.terserOptions.compress.drop_console = true
    }
  }
}
