export default {
  scene: null, // 场景对象
  camera: null, // 相机
  controls: null, // 控制器 模型 旋转、移动、缩放
  pointLight: null, // 点光源对象
  stats: null,
  composer: null,
  renderer: null, // 渲染器
  mixer: null,
  outlinePass: null,
  renderPass: null,
  selectedObjects: [],
  clock: null, // 时钟对象
  object3DLists: null, // 存储模型
  animationList: [], // 存储动画
  demo: []
}
