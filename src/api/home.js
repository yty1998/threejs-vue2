import { request, request2 } from '@/network/request'

// agv
export function agv() {
  return request({
    url: '/agv/'
  })
}
// tedata
export function tedata() {
  return request2({
    url: '/getTeStatusData'
  })
}
// teap
export function teap() {
  return request({
    url: '/teap/'
  })
}
// smtuidata
export function smtuidata() {
  return request({
    url: '/smtuidata/'
  })
}
