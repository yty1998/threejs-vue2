import axios from 'axios'
export function request(config) {
  // eslint-disable-next-line new-cap
  const instance = new axios.create({
    baseURL: '/data1', // 地址
    timeout: 5000, // 超时时间
    method: config.method === undefined ? 'get' : config.method, // 请求方法
    withCredentials: true, // 如果是跨域请求 接口文档要求
    headers: {
      Connection: 'keep-alive'
    }
  })

  // axios 请求拦截
  // 就是在请求数据时对数据做一些处理
  instance.interceptors.request.use(config => {
    return config
  }, err => {
    console.log(err)
  })

  // axios 响应拦截
  // 对返回的数据做一些处理
  instance.interceptors.response.use(res => {
    return res.data
  }, err => {
    console.dir(err)
  })

  return instance(config)
}

export function request2(config) {
  // eslint-disable-next-line new-cap
  const instance = new axios.create({
    baseURL: '/three-api', // 地址
    timeout: 5000, // 超时时间
    method: config.method === undefined ? 'get' : config.method, // 请求方法
    withCredentials: true, // 如果是跨域请求 接口文档要求
    headers: {
      Connection: 'keep-alive'
    }
  })

  // axios 请求拦截
  // 就是在请求数据时对数据做一些处理
  instance.interceptors.request.use(config => {
    return config
  }, err => {
    console.log(err)
  })

  // axios 响应拦截
  // 对返回的数据做一些处理
  instance.interceptors.response.use(res => {
    return res.data
  }, err => {
    console.dir(err)
  })

  return instance(config)
}
